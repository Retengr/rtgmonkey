# RtgMonkey

A simple chaos monkey implementation for Kubernetes.

This basic implementation of the well known [chaos monkey from netflix](https://en.wikipedia.org/wiki/Chaos_engineering) was created as a complete exercice of my Kubernetes Training ([Program in french](https://www.retengr.com/formations-devops/kubernetes/)).

This implementation makes possible to deploy multiple ChaosMonkey resources in your cluster : ChaosMonkey components has its own configuration : For each, you decide wich labels must be set on its future victims, and the delay between two attacks (only one container is killed  at a time).

## Schema

![schema](doc/schema.png]

## Install


- Declare the chart registry :
  ```
  helm repo add rtg-chart http://chartmuseum.retengr.com
  helm dep update
  ```

- Install the application :
  ```
  helm install chaos rtg-chart/rtgmonkey
  ```

- Install demo
  ```
  helm install rtgmonkey-demo rtg-chart/rtgmonkey-demo
  ```


## Uninstall

```
helm uninstall your_app_name
````


## Initialisation of a ChaosMonkey

A `ChaosMonkey` is declared as a Custom Resource, You can delare it using the following syntax :

```killer1
cat << EOF | kubectl apply -f -
apiVersion: stable.rtg.com/v1
kind: RtgMonkey
metadata:
  name: killer1
spec:
  delay: 10           # Delay between 2 attacks
  podSelectors:       # List of labels that must be set on you victims pods
    -  chaosmonkey: back 
       app: demo1
    -  chaosmonkey: front
       app: demo1
EOF
```

> In the previous example, only Pods with  (`chaosmonkey: back` AND `app: demo1`)  **OR** (`chaosmonkey: front` AND `app: demo1`) might be killed by `killer1`.

Once deployed, ChaosMonkey needs two minutes to initiate its process
