import requests
import os
import json
import logging
import sys
import time
import random
from datetime import datetime


from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler



log = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
log.addHandler(out_hdlr)
log.setLevel(logging.INFO)


base_url = "https://kubernetes"


namespace = os.getenv("res_namespace", "default")
token = os.getenv("TOKEN")

header = {"Authorization": "Bearer {}".format(token)}

all_ressources={}

def searchPodsWithLabel(labels):

        url = "{}/api/v1/namespaces/{}/pods?labelSelector={}".format(
                        base_url, namespace, labels.pop(0))

        for l in labels:
            url = "{},{}".format(
                        url, l)

        r = requests.get(url,headers=header,verify='/var/run/secrets/kubernetes.io/serviceaccount/ca.crt')
        
        response = r.json()

        names = [p['metadata']['name'] for p in response['items']]
        
        return names



def randomKill(ns,chaos_name,chaos_pods_list):
    log.info('\n\nStarting random kill for {}'.format(chaos_name))

    if (len(chaos_pods_list) !=0):
        podToKill = random.choice(chaos_pods_list)
        text = 'Killing pod \'{}\' (ChaosMonkey name :{} )'.format(podToKill , chaos_name)
        url = '{}/api/v1/namespaces/{}/pods/{}'.format(base_url, ns,podToKill)
        log.info('url : {}'.format(url))

        r = requests.delete(url, stream=True,headers=header,verify='/var/run/secrets/kubernetes.io/serviceaccount/ca.crt') 
        log.info("Done")
    else:
        log.info("Nobody to kill. Leaving")



 
def event_loop():
    log.info("Starting the operator service")
    url = '{}/apis/stable.rtg.com/v1/namespaces/{}/chaosmonkeys?watch=true'.format(base_url, namespace)
    r = requests.get(url, stream=True,headers=header,verify='/var/run/secrets/kubernetes.io/serviceaccount/ca.crt')
    
    for line in r.iter_lines():
        obj = json.loads(line)

        #log.info(obj)
       
        event_type = obj['type']
        chaosmonkey_name = obj["object"]["metadata"]["name"]
        chaosmonkey_delay = obj["object"]["spec"]["delay"]

        log.info('Resources found : {} / {} / {}'.format(event_type,chaosmonkey_name, chaosmonkey_delay))

        if ( "ADDED" != event_type ): # Si ce n'est pas un ajout, je le supprime, pour le recréer ensuite
            scheduler.remove_job(chaosmonkey_name) 

        if ( "DELETED" != event_type ): # Je rajoute la ressource sauf si elle a été supprimmée :)
            labels = obj["object"]["spec"]["podSelectors"]
 
            allGroupedLabels=[]

            for l in labels:
                result = []
                for k in l.keys():
                 result.append(k+"="+l.get(k))
                allGroupedLabels.append(result)

            pods_name = []
            for lg in allGroupedLabels:
                podsNamesForOneLabelGroup = searchPodsWithLabel(lg)
                for n in podsNamesForOneLabelGroup:
                    pods_name.append(n)


            scheduler.add_job(randomKill,'interval',args=[namespace,chaosmonkey_name,pods_name], seconds=chaosmonkey_delay,id=chaosmonkey_name,replace_existing=True)
          

            log.info('Killer {} / Delayed {} seconds. Concerned pods :{}'.format(chaosmonkey_name,chaosmonkey_delay,pods_name))
            
        jobs = scheduler.get_jobs()
        
     


scheduler = BackgroundScheduler()
scheduler.start()
print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

try:
    while True:
        event_loop()
        print('Restarting the loop')

except (KeyboardInterrupt, SystemExit):
    # Not strictly necessary if daemonic mode is enabled but should be done if possible
    scheduler.shutdown()

