#!/bin/sh

export THEKILLERVERSION=0.0.2
export CHARTVERSION=0.0.18
export CHARTAPPVERSION=0.0.6
sed -i.bak 's/__THEKILLERVERSION__/'"$THEKILLERVERSION"'/g' thekiller/build_image.sh
sed -i.bak 's/__THEKILLERVERSION__/'"$THEKILLERVERSION"'/g' values.yaml
sed -i.bak 's/__CHARTVERSION__/'"$CHARTVERSION"'/g' Chart.yaml
sed -i.bak 's/__CHARTAPPVERSION__/'"$CHARTAPPVERSION"'/g' Chart.yaml
